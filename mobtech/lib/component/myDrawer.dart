import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Mohamad Kattaa"),
            accountEmail: Text("mohamadkattaa@yahoo.com"),
            currentAccountPicture: CircleAvatar(
              child: Icon(Icons.person),
            ),
            decoration: BoxDecoration(
                color: Colors.red,
                image: DecorationImage(
                    image: AssetImage(
                      'images/brain.jpg',
                    ),
                    fit: BoxFit.cover)),
          ),
          ListTile(
            title: Text(
              "الصفحة الرئيسية",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal),
            ),
            leading: Icon(
              Icons.home,
              color: Colors.red,
              size: 25,
            ),
            trailing: Icon(Icons.hot_tub),
            // subtitle: Text(""),
            // isThreeLine: true,
            // dense: true, // smaller Tilte
            // contentPadding: EdgeInsets.all(10),
            onTap: () {
              print("tap");
              Navigator.of(context).pushNamed("home");
            },
            /* onLongPress: () {
                      print("long");
                    }, */
          ),
          ListTile(
            title: Text(
              "الاقسام",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal),
            ),
            leading: Icon(
              Icons.category,
              color: Colors.red,
              size: 25,
            ),
            onTap: () {
              print("tap2");
              Navigator.of(context).pushNamed("categories");
            },
          ),
          Divider(
            color: Colors.red,
            height: 20,
            thickness: 2,
          ),
          ListTile(
            title: Text(
              "حول التطبيق",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal),
            ),
            leading: Icon(
              Icons.info,
              color: Colors.red,
              size: 25,
            ),
            onTap: () {
              print("tap2");
            },
          ),
          ListTile(
            title: Text(
              "الاعدادات",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal),
            ),
            leading: Icon(
              Icons.settings,
              color: Colors.red,
              size: 25,
            ),
            onTap: () {
              print("tap2");
            },
          ),
          ListTile(
            title: Text(
              "تسجيل الخروج",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal),
            ),
            leading: Icon(
              Icons.exit_to_app,
              color: Colors.red,
              size: 25,
            ),
            onTap: () {
              print("tap2");
            },
          ),
        ],
      ),
    );
  }
}
