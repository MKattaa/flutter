import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobtech/pages/categories.dart';
import 'package:mobtech/pages/home.dart';
import 'package:mobtech/pages/samsung.dart';
import 'sideBar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    sideBar();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // start
      title: 'Mobtech',
      theme: ThemeData(fontFamily: 'Cairo'),
      home: Home(),
      routes: {
        'home': (context) {
          return Home();
        },
        'categories': (context) {
          return Categories();
        },
        'samsung': (context) {
          return Samsung();
        },
      },
      // End
    );
  }
}
