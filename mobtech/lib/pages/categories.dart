import 'package:flutter/material.dart';
import 'package:mobtech/component/myDrawer.dart';

class Categories extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CategoriesState();
  }
}

class CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    var fill = BoxFit.fill;
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("الاقسام"),
          backgroundColor: Colors.red,
        ),
        drawer: MyDrawer(),
        body: GridView(
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          children: [
            InkWell(
              child: Card(
                // color: Colors.black26,
                child: Column(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "images/category/samsung.png",
                        fit: fill,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Samsung",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                Navigator.of(context).pushNamed("samsung");
              },
            ),
            Container(
              height: 200,
              child: Card(
                // color: Colors.black26,
                child: Column(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "images/category/samsung.png",
                        fit: fill,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Samsung",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 200,
              child: Card(
                // color: Colors.black26,
                child: Column(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "images/category/samsung.png",
                        fit: fill,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Samsung",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 200,
              child: Card(
                // color: Colors.black26,
                child: Column(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "images/category/samsung.png",
                        fit: fill,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Samsung",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
