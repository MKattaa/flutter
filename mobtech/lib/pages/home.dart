import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:mobtech/component/myDrawer.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Home"),
          backgroundColor: Colors.red,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: () {})
          ],
          centerTitle: false,
          elevation: 10,
          /* leading: IconButton(
                icon: Icon(Icons.search),
                onPressed: () {},
              ), */
          titleSpacing: 20,
          brightness: Brightness.light,
          // primary: false,
        ),
        /* enddrawer: Drawer(), */
        drawer: MyDrawer(),
        /*floatingActionButton: IconButton(
            icon: Icon(Icons.shopping_basket),
            onPressed: () {},
            ),*/
        body: ListView(
          children: [
            SizedBox(
                height: 150.0,
                width: double.infinity,
                child: Carousel(
                  images: [
                    /* Image.asset(
                          'images/analysis.jpg',
                          fit: BoxFit.cover,
                        ), */
                    AssetImage(
                      'images/brain.jpg',
                    ),
                    AssetImage(
                      'images/analysis.jpg',
                    ),
                  ],
                  dotSize: 8,
                  dotIncreaseSize: 2,
                  dotSpacing: 20,
                  dotColor: Colors.red,
                  dotBgColor: Colors.black.withOpacity(0.5),
                  showIndicator: true,
                  indicatorBgPadding: 10,
                  boxFit: BoxFit.cover,
                  borderRadius: true,
                  radius: Radius.circular(10),
                  overlayShadow: true,
                  overlayShadowColors: Colors.black,
                  overlayShadowSize: 0.3,
                )),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "أقسام",
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 20,
                  color: Colors.red,
                ),
              ),
            ),
            Container(
              height: 120,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    child: ListTile(
                      title: Image.asset(
                        "images/category/ins.png",
                        width: 80,
                        height: 80,
                      ),
                      subtitle: Text(
                        "Instagram",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "اخر المنتجات",
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 20,
                  color: Colors.red,
                ),
              ),
            ),
            Container(
              height: 400,
              child: GridView(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                children: [
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                  InkWell(
                    child: GridTile(
                      child: Image.asset("images/product/p30.jpg"),
                      footer: Container(
                        height: 20,
                        color: Colors.red.withOpacity(0.5),
                        child: Text(
                          "P30 Pro",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    onTap: () {
                      print("object InkWell");
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
