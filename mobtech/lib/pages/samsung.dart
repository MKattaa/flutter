import 'package:flutter/material.dart';

class Samsung extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SamsungState();
  }
}

class SamsungState extends State<Samsung> {
  var mobileList = [
    {'name': 'S20 Ultra', 'camera': '5 ميغا', 'cpu': 'ثماني', 'price': '1100'}
  ];
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("سامسونج"),
          backgroundColor: Colors.red,
        ),
        body: ListView.builder(
          itemCount: mobileList.length,
          itemBuilder: (context, i) {
            return Container(
              height: 100,
              child: Card(
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Image.asset("images/product/p30.jpg"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: 100,
                        child: Column(
                          crossAxisAlignment:
                              CrossAxisAlignment.start, // Vertical
                          children: [
                            Text(
                              mobileList[i]['name'],
                              style: TextStyle(fontWeight: FontWeight.w800),
                              textAlign: TextAlign.center,
                            ),
                            Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.start, // horizontal
                              children: [
                                Expanded(
                                  child: Text(
                                    "الكاميرا: " + mobileList[i]['camera'],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                                Expanded(
                                  child: Text(
                                    "المعالج: " + mobileList[i]['cpu'],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text(
                                "السعر : ${mobileList[i]['price']} \$",
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
